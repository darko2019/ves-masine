const staticCacheName = 'site-static-v1.4';
const dynamicCacheName = 'site-dynamic-v1.4';
const assets = [

    // u produkciji skidamo  'http://localhost:63342/ves-masine'
    '/',
    '/images/logo/extem-logo.png',
    '/images/logo/extem-logo.webp',
    '/index.html',
    '/fallback.html',
    '/js/main.js',
    '/css/style.css',
    '/images/logo/icon-72x72.png',
    'https://fonts.googleapis.com/css2?family=Trirong:ital@0;1&display=swap',
    'https://code.jquery.com/jquery-3.5.1.slim.min.js',
    'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css',
    'https://unpkg.com/aos@2.3.1/dist/aos.css',
    'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js',
    '/images/ves-masine.webp',
    '/images/servis-bele-tehnike-elektricni-sporeti.webp',
    'images/servis-bele-tehnike-sudomasine.webp'
];

// install service worker
self.addEventListener('install', evt => {
    // console.log('service worker has been installed');
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
            console.log('caching shell assets');
            cache.addAll(assets);
        })
    );
});

// activate service worker
self.addEventListener('activate', evt => {
    evt.waitUntil(caches.keys().then(keys => {
        // console.log(keys)
        return Promise.all(keys
            .filter(key => key !== staticCacheName && key !== dynamicCacheName)
            .map(key => caches.delete(key)))
    }));
    // console.log('service worker has been activated');
});

// fetch events
self.addEventListener('fetch', evt => {
    // console.log('fetch event', evt);
    evt.respondWith(
        caches.match(evt.request).then(cacheRes => {
            return cacheRes || fetch(evt.request).then(fetchRes => {
                return caches.open(dynamicCacheName).then(cache => {
                    cache.put(evt.request.url, fetchRes.clone());
                    return fetchRes;
                });
            });
        }).catch(
            () => {
                if (evt.request.url.indexOf('.html') > -1) {
                    return caches.match(
                        '/fallback.html')
                }
            })
    );
});