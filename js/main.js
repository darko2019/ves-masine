let navTabSvi = document.querySelectorAll(".nav-onclick");
let vesMasine = ' Servis veš mašina';
let belaTehnika = ' Servis bele tehnike ';
let blog = ' Blog ';
let vesMasineHref = "https://servisvesmasine.rs/";
let belaTehnikaHref = "servis-bele-tehnike.html";
let blogHref = "blog-extem-servis.html";
let hamburgerWidth = 991;

// Menjanje stranica u navigaciji na klik na tabove Servis ves masine, Servis bele tehnike i Blog za tabove bez hamburgera
if (window.innerWidth >= hamburgerWidth) {
  navTabSvi.forEach(
      box => box.addEventListener("click", (event) => {
        switch (event.target.innerText) {
          case vesMasine:
            location.href = vesMasineHref;
            break;
          case belaTehnika:
            location.href = belaTehnikaHref;
            break;
          case blog:
            location.href = blogHref;
            break;
          default:
            location.href = vesMasineHref;
        }
      }));
}
//Progressive Web App
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('./sw.js')
  .then((reg) => console.log('service worker registered', reg))
  .catch((err) => console.log('service worker not registered', err));
}
// SCROLL TO TOP
document.addEventListener("DOMContentLoaded", function (event) {

  const button = document.querySelector('#scroll-top');

  button.addEventListener('click', function () {
    window.scrollTo({top: 0, left: 0, behavior: "smooth"});
  });

  // window.addEventListener('scroll', function () {
  //     if (window.scrollY < 200) {
  //         button.style.opacity = "0";
  //     } else {
  //         button.style.opacity = "1";
  //     }
  // });

});

